async function init() {
  let rustApp = null

  // import rust before listening to events
  try {
    rustApp = await import('../pkg') // importing web assembly file
  } catch(err) {
    console.error(err)
    return;
  }

  console.log(rustApp)

  const input = document.getElementById('upload')
  const fileReader = new FileReader()

    // read file data when the file is loaded
  fileReader.onloadend = () => {
    // removes meta data
    let base64 = fileReader.result.replace(
      /^data:image\/(png|jpeg|jpg);base64,/, ''
    )
    // compare values
    // console.log(input.files)[0]
    // console.log(base64)
    let img_data_url = rustApp.grayscale(base64)
    document.getElementById('new-img').setAttribute(
      'src', img_data_url
    )
  }

  // fired whenever user uploads file
  input.addEventListener('change', () => {
    // input.files[0] - returns a file object, fileReader.result - returns a string
    // convert binary file to string
    fileReader.readAsDataURL(input.files[0]) // fileReader is a class from the browser, allowing to store files in js
  })
}

init()