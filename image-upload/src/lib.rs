use wasm_bindgen::prelude::*;
use web_sys::console::log_1 as log;
use base64::decode;

#[wasm_bindgen]
// expose method to js by making it public and using webassembly bind crate
pub fn grayscale(encoded_file: &str) {
    log(&"Grayscale called".into());
    // convert base64 into vector
    let base64_to_vector = decode(encoded_file).unwrap();
    log(&"Image decoded".into());
    // log(&encoded_file.into()) // calling js function so it needs type converted from string to js value
}