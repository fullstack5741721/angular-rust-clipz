<h1 align="center">
<br>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/angular/angular-original.svg" width="80px" />
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/rust/rust-original.svg" width="70px" />
<br>
<br>
Gaming highlights application
</h1>
<p>Self-hosting video application where users can upload and share their gaming highlights with their friends.</p>
<h4 align="left">Angular/Typescript/RxJS/Tailwind/Web Assembly/Rust</h4>

<div>
    <img src="./img/clipz1.png" width="100%" height="380px">
      <img src="./img/clipz2.png" width="100%" height="380px">
        <img src="./img/clipz3.png" width="100%" height="380px">
    <div style="display: inline-block; text-align: left;">
        <h3>Client Side</h3>
        <ul>
            <li>Angular Routing, Directives, Pipes, Services</li>
            <li>RxJS</li>
            <li>Tailwind</li>
        </ul>
    </div>
    <img src="./img/upload.png" width="100%" height="380px">
    <div style="display: inline-block; text-align: left; margin-left: 40px;">
        <h3>Backend</h3>
        <ul>
            <li>Firebase for API/Database</li>
            <li>Image processing with rust and web assembly - applies a greyscale effect to an image. Webpack is used as the development server/compiling rust and bundling</li>
        </ul>
    </div>
</div>
