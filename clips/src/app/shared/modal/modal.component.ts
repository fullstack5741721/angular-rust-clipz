import { Component, Input, OnInit, ElementRef } from '@angular/core';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrl: './modal.component.css',
})
export class ModalComponent implements OnInit {
  @Input() modalID = '';
  // inject ModalService instance
  // ElementRef - // gives us access to host element of the component - helps to achieve a portal to prevent css child inherting, goal is to transfer the component location to the document root(the body tag)
  constructor(public modal: ModalService, public el: ElementRef) {
    // console.log(el);
  }

  ngOnInit(): void {
    // moves the modal to the root
    document.body.appendChild(this.el.nativeElement);
  }

  closeModal() {
    this.modal.toggleModal(this.modalID);
  }
}
