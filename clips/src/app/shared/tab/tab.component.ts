import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css'],
})
export class TabComponent implements OnInit {
  // default tab title and active values
  @Input() tabTitle = '';
  @Input() active = false; // renders content based on the active tab

  constructor() {}

  ngOnInit(): void {}
}
