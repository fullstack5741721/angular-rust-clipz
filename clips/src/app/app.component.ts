import { Component } from '@angular/core';
// import { ModalService } from './services/modal.service';
// import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  // standalone: true,
  // imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  // testing memory leak for modal - checking modal is unregistered after its been destroyed
  // showModal = true;
  // find modals with duplicate id's, the original modal isnt destoryed in the service therefroe the browser is managing modals that dont exist anymore
  // constructor(public modal: ModalService) {}
  // ngOnInit() {
  //   setInterval(() => {
  //     this.showModal = !this.showModal;
  //     console.log(this.modal.modals);
  //   }, 1000);
  // }
}
