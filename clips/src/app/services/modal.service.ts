import { Injectable } from '@angular/core';

interface IModal {
  id: string; // help the service identify the modal
  visible: boolean; // show/hide modal
}

// tell angular to inject the class into the component using it
@Injectable({
  providedIn: 'root', // tells angular to expose the service to root (inject to any component)
})
export class ModalService {
  // private visible = false;
  private modals: IModal[] = []; // push new modals into the array

  constructor() {}

  // regiter a new modal
  register(id: string) {
    this.modals.push({
      id,
      visible: false, // hide modal by default
    });
    // console.log(this.modals);
  }

  isModalOpen(id: string): boolean {
    // true/false for showing modal
    // return this.visible;
    // service no longer manages single modal, searches through arr for the id
    return !!this.modals.find((el) => el.id === id)?.visible; // convert non boolean value to boolean
  }

  unregister(id: string) {
    // unregister to prevent memory leaks
    this.modals = this.modals.filter((el) => el.id !== id);
  }

  toggleModal(id: string) {
    const modal = this.modals.find((el) => el.id === id);
    if (modal) {
      modal.visible = !modal.visible;
    }
    // this.visible = !this.visible;
  }
}
